import keras
from keras.layers import Dense, Flatten, Input
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Model
from keras import backend as K
import numpy as np
from transformer.spatial_transformer import transformer


class TransformerDetector(object):

    """Docstring for TransformerDetector. """

    def __init__(self, loc_only=True, num_classes=None):
        """Constructor """
        self.loc_only = loc_only
        if not self.loc_only:
            self.num_classes = num_classes

    def preprocess(self, inputs):
        """Preprocess inputs

        :inputs: TODO
        :returns: TODO

        """
        return inputs

    def predict(self, preprocessed_inputs):
        """Predicts unpostprocessed tensors from input tensor.

        :inputs: TODO
        :returns: TODO

        """
        image_input = Input(shape=preprocessed_inputs['image_input'].shape[1:],
                            name='image_input')
        x = MaxPooling2D((2, 2), name='loc_feat_1')(image_input)
        x = Conv2D(20, (5, 5), kernel_initializer='he_uniform',
                   name='loc_feat_2')(x)
        x = MaxPooling2D((2, 2), name='loc_feat_3')(x)
        x = Conv2D(20, (5, 5), kernel_initializer='he_uniform',
                   name='loc_feat_4')(x)
        x = Flatten(name='flatten_loc_features')(x)
        if 'anchor_point' in preprocessed_inputs:
            anchor_point_input = Input(
                shape=preprocessed_inputs['anchor_point_input'].shape[1:],
                name='anchor_point_input')
            x = keras.layers.concatenate(
                [x, anchor_point_input], axis=1,
                name='concat_loc_features_anchor_point')
        x = Dense(50, kernel_initializer='he_uniform', activation='relu',
                  name='loc_dense_1')(x)
        b = np.zeros((2, 3), dtype=K.floatx())
        b[0, 0] = 1
        b[1, 1] = 1
        W = np.zeros((50, 6), dtype=K.floatx())
        weights = [W, b.flatten()]
        x = Dense(6, weights=weights, name='loc_dense_2')(x)
        if not self.loc_only:
            x = self._predict_class(preprocessed_inputs, image_input, x)
        if 'anchor_point' in preprocessed_inputs:
            model = Model(inputs=[image_input, anchor_point_input],
                          outputs=x)
        else:
            model = Model(inputs=image_input, outputs=x)
        return model

    def _predict_class(self, preprocessed_inputs, image_input, loc_output):
        new_shape = preprocessed_inputs['image_input'].shape[1:]
        new_shape = (int(new_shape[0]/2), int(new_shape[1]/2))
        x = transformer(image_input, loc_output, new_shape)
        x = Conv2D(32, (3, 3), kernel_initializer='he_uniform',
                   activation='relu', name='classif_feat_2')(x)
        x = MaxPooling2D((2, 2), name='classif_feat_3')(x)
        x = Conv2D(32, (3, 3), kernel_initializer='he_uniform',
                   activation='relu', name='loc_feat_4')(x)
        x = MaxPooling2D((2, 2), name='loc_feat_5')(x)
        x = Flatten(name='flatten_classif_features')(x)
        x = Dense(256, kernel_initializer='he_uniform', activation='relu',
                  name='classif_dense_1')(x)
        x = Dense(self.num_classes, kernel_initializer='he_uniform',
                  activation='softmax', name='classif_dense_2')(x)
        return x

    def postprocess(self, prediction_dict):
        """Convert prediction tensors to final detections.

        :prediction_dict: TODO
        :returns: TODO

        """
        pass

    def loss(self, model):
        """Compute scalar loss tensors given prediction tensors

        :prediction_dict: TODO
        :returns: TODO

        """
        if self.loc_only:
            model.compile(optimizer='adam', loss='mean_squared_error')
        else:
            model.compile(optimizer='adam', loss='categorical_crossentropy')
        pass
